# Generate a random number between 1 and 9 (including 1 and 9). Ask the user to guess the number,
# then tell them whether they guessed too low, too high, or exactly right. 
# (Hint: remember to use the user input lessons from the very first exercise
import random
num = random.randint(1,9)

print("Guess the number")

result = 0

while(result != num):
    result = int(input())
    if result == num:
        print("Nice guess!")
        break
    elif result > num: 
        print("Too low")
    elif result < num:
        print("Too high")