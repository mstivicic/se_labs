import random

list = ["rock", "paper", "scissors"]
item = random.choice(list)

while True:
    print("Rock, paper, scissors")
    print("Choose: ")
    yourChoice = input()
    print("Comp picked: " + item)
    if yourChoice == "rock" and item == "rock":
        print("Draw")
    elif yourChoice == "rock" and item == "paper":
        print("You lose!")
    elif yourChoice == "rock" and item == "scissors":
        print("You won!")
    elif yourChoice == "scissors" and item == "scissors":
        print("Draw")
    elif yourChoice == "scissors" and item == "rock":
        print("You lose!")
    elif yourChoice == "scissors" and item == "paper":
        print("You won!")
    elif yourChoice == "paper" and item == "paper":
        print("Draw")
    elif yourChoice == "paper" and item == "scissors":
        print("You lose!!")
    elif yourChoice == "paper" and item == "rock":
        print("You won!")
    cont = input("Continue? yes/no")
    if cont == "no":
        break    
