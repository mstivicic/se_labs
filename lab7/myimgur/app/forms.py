from django.db.models.base import Model
from .models import Image, Comment
from django.forms import ModelForm
from bootstrap_datepicker_plus.widgets import DateTimePickerInput

class ImageForm(ModelForm):
    class Meta:
        model = Image
        fields=['title','url','description','pub_date']
        widgets = {
            'pub_date': DateTimePickerInput(),
        }

class CommentForm(ModelForm):
    class Meta:
        model = Comment
        fields = ['text']
