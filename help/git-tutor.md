# Git tutor

# Emanuel Maricic

### git status

- govori nam ima li nekakvih promjena fileova u repozitoriju

### git add

- dodaje file git bashu tako da moze pratiti promjene u tom fileu

### git log

- prikazuje listu svih commitova s njihovim komentarima i datumom izmjene

### git commit

- sprema izmjene na projektu u sustav za verzioniranje

### git diff

- govori nam u kojoj liniji koda je napravljena promjena

### git push

- služi da postavimo projekt u napravljeni online repozitorij

### git pull

- provlači promjene sa udaljenog repozitorija na lokalni repozitorij

### git config

- služi za postavljanje svakakvih konfiguracija kao što su username ili email

### git commit -a -m ""

- naredba za zaobici git add
